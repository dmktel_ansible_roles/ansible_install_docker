Role Name
=========

Install docker

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

tag: 1.0.0

|           Varible            | Required |                           Default                            |     Comment      |
| :--------------------------: | :------: | :----------------------------------------------------------: | :--------------: |
| docker_requirements_packages |   yes    |                        packages list                         | example variable |
|        docker_key_url        |   yes    | [https://download.docker.com/linux/ubuntu/gpg](https://download.docker.com/linux/ubuntu/gpg) | example variable |
|       docker_repo_url        |   yes    |       [deb https://download.docker.com/linux/ubuntu]()       | example variable |
|       docker_repo_dist       |   yes    |                            focal                             | example variable |
|     docker_repo_version      |   yes    |                            stable                            | example variable |
|   docker_install_packages    |   yes    |                        packages list                         | example variable |
|         docker_user          |   yes    |                            ubuntu                            | example variable |
|         docker_group         |   yes    |                            docker                            | example variable |

tag: 1.0.1 - default docker_user: vagrant

|           Varible            | Required |                     Default                      |     Comment      |
| :--------------------------: | :------: | :----------------------------------------------: | :--------------: |
| docker_requirements_packages |   yes    |                  packages list                   | example variable |
|        docker_key_url        |   yes    |   https://download.docker.com/linux/ubuntu/gpg   | example variable |
|       docker_repo_url        |   yes    | [deb https://download.docker.com/linux/ubuntu]() | example variable |
|       docker_repo_dist       |   yes    |                      focal                       | example variable |
|     docker_repo_version      |   yes    |                      stable                      | example variable |
|   docker_install_packages    |   yes    |                  packages list                   | example variable |
|         docker_user          |   yes    |                     vagrant                      | example variable |
|         docker_group         |   yes    |                      docker                      | example variable |

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
